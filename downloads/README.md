## Download Sample Application

This folder contains executables for different plattforms:
* [Windows 64 Bit](https://gitlab.com/sbaeumlisberger/virtualizing-wrap-panel/raw/master/downloads/VirtualizingWrapPanelSamples-win-x64.exe)
* [Windows 32 Bit](https://gitlab.com/sbaeumlisberger/virtualizing-wrap-panel/raw/master/downloads/VirtualizingWrapPanelSamples-win-x86.exe)

Alternatively, you can clone the repository and build the samples yourself.